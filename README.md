# Code Planner

    clone ....
    cd ./code-planner-web

## Copy .example.env to .env

    cp .example.env .env

## Install Dependencies

    yarn

## Run Up Project on local server

    yarn start

## Build Project

    yarn build

# Support Docker Image

[Dockerfile](#Dockerfile)

## Run on Docker

    docker build -t code-planner-web  .
    docker run  -p 8081:80 code-planner-web:latest

## .env
- AES with secret key(cp-aes-frontend) to encrypt data: 
    
     {"domain":"localhost","api":"http://localhost:8080"} 
