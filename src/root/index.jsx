import React from 'react'
import { render } from 'react-dom'
import App from "../app/views/layouts/app"


import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'
import configureStore from "../app/state/store"

const reduxStore = configureStore(window.REDUX_INITIAL_DATA)
const Title = "Code Planer"
const Root = (
    <Provider store={reduxStore}>
        <PersistGate loading={null} persistor={persistStore(reduxStore)}>
            <App />
        </PersistGate>
    </Provider>
)

render(Title, document.getElementById("title"))
render(Root, document.getElementById("root"))