import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PrivateRoute from "../containers/PrivateRoute";
import styles from "./styles.css";
import { withStyles } from "@material-ui/styles";
import { connect } from "react-redux";
import { sessionOperations } from "../../state/ducks/session";

import Dashboard from "../pages/dashboard";
// import Login from "../pages/login";
import Verification from "../pages/verification";

function App(props) {
    const { isAuth } = props;

    return (
        <Router>
            <Switch>
                <Route path='/verify'>
                    <Verification />
                </Route>
                {/* <Route path='/login'>
                    <Login />
                </Route> */}
                {/* Must put the private route as last order */}
                <Route path='/' >
                    <Dashboard />
                </Route>
            </Switch>
        </Router >
    );
}

const mapStateToProps = state => ({
    isAuth: state.session.isAuth,
    isLoading: state.session.isLoading,
});

const mapDispatchToProps = dispatch => ({
    info: () => {
        dispatch(sessionOperations.info());
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(App));
