import React, { useEffect } from 'react';
import AppMenu from '../components/AppMenu';
import { connect } from 'react-redux';
import { frontendOperations } from '../../state/ducks/frontend';
import { sessionOperations } from '../../state/ducks/session';

function AppMenuContainer(props) {
	let handleExtend = index => e => {
		props.extendAppMenu(index);
	};
	let handleLogoutAction = e => {
		props.reset();
		props.logout();
	};
	useEffect(() => {
		props.getAppMenu();
	});

	return (
		<AppMenu
			{...props}
			handleExtend={handleExtend}
			handleLogoutAction={handleLogoutAction}
		/>
	);
}

const mapStateToProps = state => ({
	struct: state.frontend.appMenuStruct,
	isAuth: state.session.isAuth
});
const mapDispatchToProps = dispatch => ({
	reset: () => dispatch(frontendOperations.resetApp()),
	extendAppMenu: index => dispatch(frontendOperations.extendAppMenu(index)),
	getAppMenu: () => dispatch(frontendOperations.getAppMenu()),
	logout: () => dispatch(sessionOperations.logout()),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AppMenuContainer);
