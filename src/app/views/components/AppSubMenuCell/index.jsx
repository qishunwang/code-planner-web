import React from 'react'
import './styles.css'

export const AppSubMenuCell = (props) => {
    const { item } = props

    return (
        <div className={"container"}>
            <img src={item.img} />
            <div >
                {item.title}
            </div>
        </div>
    )
}