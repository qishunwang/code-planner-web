import styled from "styled-components";
import { Link } from "react-router-dom";

export const StyledLink = styled(Link)`
    color: #ffffff;
    text-decoration: none;
    cursor: pointer;

    :hover {
        color: #b1d7ff;
    }
`;
