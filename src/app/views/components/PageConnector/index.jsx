import React from "react"
import { useRef, useLayoutEffect, useState } from "react"


export default class PageConnector extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            present: false,
            windowWidth: window.innerWidth,
            windowHeight: window.innerHeight,
            dimensions: { width: 0, height: 0, offsetTop: 0, offsetLeft: 0, offsetRight: 0 }
        }
        this.targetRef = React.createRef()
    }
    updateDimensions = () => {
        const targetRef = this.targetRef
        this.setState({
            dimensions: {
                width: targetRef.current.offsetWidth,
                height: targetRef.current.offsetHeight,
                offsetTop: targetRef.current.offsetTop,
                offsetLeft: targetRef.current.offsetLeft,
            }
        })
    }
    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions)
        this.updateDimensions()
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions)
    }

    handleBack = () => {
        this.setState({ present: false })
    }
    handleNext = () => {
        this.setState({ present: true })
    }

    render() {
        const { From, To } = this.props
        const {
            present,
            dimensions
        } = this.state

        return <div ref={this.targetRef}>
            <From handleNext={this.handleNext} handleBack={this.handleBack} />
            {/* size
<p>{dimensions.width}</p>
<p>{dimensions.height}</p>
offset
<p>{dimensions.offsetTop}</p>
<p>{dimensions.offsetLeft}</p> */}
            <div style={{
                position: "absolute",
                top: dimensions.offsetTop,
                left: dimensions.offsetLeft,
                width: dimensions.width
            }}>
                <To present={present} handleNext={this.handleNext} handleBack={this.handleBack} />
            </div>
        </div>

        // return <Wrapper
        //     windowWidth={windowWidth}
        //     windowHeight={windowHeight}
        //     From={From}
        //     handleNext={this.handleChange}
        //     To={To}
        //     present={present}
        //     handleBack={this.handleChange} />
    }
}