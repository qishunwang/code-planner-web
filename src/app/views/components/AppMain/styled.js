import styled from 'styled-components'

export const Main = styled.div`
float: left;
width: 75%;
padding-left: 2%;
overflow-y: scroll;
height: 100vh
::-webkit-scrollbar {
  display: none;
}
`;

export const Container = styled.div`
  padding: 20px;
    margin: 15px;
    border: 1px solid #d2d2d2;
    background: #ffffff
`;