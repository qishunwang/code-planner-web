import React from 'react'
import { Main, Container } from "./styled"


export default function AppMain(props) {
    const { classes, children } = props
    return (
        <Main >
            <Container>{children}</Container>
        </Main>
    )
} 