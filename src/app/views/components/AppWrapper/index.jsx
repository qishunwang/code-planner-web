import React from 'react'
import './styles.css'
export default function AppWrapper(props) {

    const { children } = props

    return <div  >
        {children}
    </div>
} 