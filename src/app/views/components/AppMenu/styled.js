import styled from "styled-components";
import { Link } from "react-router-dom";

export const StyledLink = styled(Link)`
    color: #ffffff;
    text-decoration: none;
    cursor: pointer;

    :hover {
        color: #b1d7ff;
    }
`;

export const Menu = styled.div`
    background-color: #000000;
    height:  100vh ;
    width: 20%;
    color: #ffffff;
    text-align: center;
    float: left;
    left: 0%;
    overflow-y: scroll;
    ::-webkit-scrollbar {
        display: none;
    }
    @media (max-width: 600px) {
        left: 0;
        min-width: 100px;
    }
`;

export const Container = styled.div`
    padding: 20px;
    margin: 15px;
    border: 1px solid #d2d2d2;
    background: #ffffff;
`;

export const FullHeader = styled.h1`
    padding: 25px;
    font-size: 2rem;
    @media (max-width: 600px) {
        display: none;
    }
`;

export const ShortHeader = styled.h1`
    display: none;
    @media (max-width: 600px) {
        display: inline-block;
    }
`;

export const MenuUL = styled.ul`
    list-style: none;
`;
export const MenuLI = styled.li`
    color: #ffffff;
    width: 100%;
    display: block;
    text-decoration: none;
    padding: 15px 0;
    border-top: 1px solid #8ba1b6;
`;

export const SubMenuUL = styled.ul`
    list-style: none;
    @media (max-width: 600px) {
        padding-left: 20px;
        z-index: 10;
    }
`;

export const LogoutButton = styled.button`
    margin: auto;
    background: #fe355f;
    border-radius: 5px;
    border-width: 0px;
    padding: 10px;
    max-width: 60px;
    cursor: pointer;
    :hover {
        background: lightYellow;
    }
`;

export const Footer = styled.div`
position: fixed;

bottom: 5px;
left: 4px;
text-align: center;
color: 'white';

@media (max-width: 1200px) {
    max-width: 200px;
}

@media (max-width: 900px) {
    max-width: 150px;
}

@media (max-width: 650px) {
    max-width: 100px;
}
`;