import React, { Fragment } from "react";
import { Link } from "react-router-dom"
// import './styles.css'
import { Menu, MenuUL, FullHeader, ShortHeader, StyledLink, SubMenuUL, LogoutButton,Footer } from "./styled";
import { AppMenuCell } from "../AppMenuCell";
import { AppSubMenuCell } from "../AppSubMenuCell";

export default function AppMenu(props) {
    const { classes, struct, handleExtend, handleLogoutAction, handleLoginAction, isAuth } = props;
    return (
        <Fragment>
            <Menu>
                <Link style={{ textDecoration: 'none', color: 'inherit' }} to="/"><FullHeader >Code Planner</FullHeader></Link>
                <Link style={{ textDecoration: 'none', color: 'inherit' }} to="/"><ShortHeader>CP</ShortHeader></Link>
                <MenuUL>
                    {struct.map((item, i) => (
                        <li key={item.title}>
                            <StyledLink to={isAuth ? item.link : "/login"} onClick={handleExtend(i)}>
                                <AppMenuCell item={item} />
                            </StyledLink>
                            {item.isExtend ? (
                                <SubMenuUL>
                                    {item.children.map((item, i) => (
                                        <li key={item.title}>
                                            <StyledLink to={isAuth ? item.link : "/login"}>
                                                <AppSubMenuCell item={item} />
                                            </StyledLink>
                                        </li>
                                    ))}
                                </SubMenuUL>
                            ) : null}
                        </li>
                    ))}
                </MenuUL>
                {
                    isAuth ?
                        <LogoutButton onClick={handleLogoutAction}>Logout</LogoutButton>
                        :
                        <StyledLink to={"/login"} ><LogoutButton >Login</LogoutButton></StyledLink>
                }
                <Footer  >
                    {"© 2019 Powered by Qi-Shun Wang."}
                </Footer>
            </Menu>

        </Fragment>
    );
}
