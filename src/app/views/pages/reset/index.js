import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { sessionOperations } from "../../../state/ducks/session";
import { useHistory, useLocation, Redirect, Link } from "react-router-dom";
import { Grid, TextField, Button } from "@material-ui/core";
import styled from "styled-components";
import { useQuery } from "../utils"

const Container = styled.div`
    padding: 40px;
`;
function Reset(props) {
    const [password, setPassword] = useState("");
    let { state: { token } } = useLocation()
    const [submited, setSubmited] = useState(false);
    const textEl = useRef(null);

    let onChange = type => e => {
        type == "password" ? setPassword(e.target.value) : null;
    };
    let reset = () => {
        setSubmited(true)
        props.reset(token, password);
    };
    useEffect(() => {
        document.title = "Reset Password";
        document.body.style.backgroundColor = "#e6f2fa";

        if (submited && props.result != null) {
            if (props.result) {
                textEl.current.innerText = "👏🏼Your password has updated!👏🏼"
                textEl.current.style.color = "green"
            } else {
                textEl.current.innerText = "🌬Your operation has expired.Please try again later💨"
                textEl.current.style.color = "red"
            }
        } else {
            textEl.current.innerText = "Confirme"
        }
        if (props.isLoading) {
            textEl.current.innerText = "Loading..."
            textEl.current.style.color = "gray"
        }
        return () => {
            document.title = "Loading...";
            document.body.style.backgroundColor = null;
        };
    }, [props.result, props.isLoading, submited]);
    return (
        <Container>
            <Grid container direction='column'>
                <TextField
                    placeholder='New Password'
                    variant='outlined'
                    style={{ margin: "0 20px 20px 0" }}
                    onChange={onChange("password")}
                />

                <Button
                    variant='outlined'
                    onClick={reset}
                    disabled={submited}
                    style={{
                        margin: "0 20px 20px 0",
                        backgroundColor: "e6f2fa",
                    }}>
                    <p style={{ fontSize: '0.8em' }} ref={textEl}></p>
                </Button>
            </Grid>
        </Container>
    );
}

const mapStateToProps = state => ({
    isLoading: state.session.isLoading,
    result: state.session.resetResult,
});

const mapDispatchToProps = dispatch => ({
    reset: (token, password) =>
        dispatch(sessionOperations.reset(token, password)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Reset);
