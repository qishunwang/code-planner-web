import styled from "styled-components";
import { Link } from "react-router-dom";


export const LogoutButton = styled.button`
    margin: auto;
    background: #fe355f;
    border-radius: 5px;
    border-width: 0px;
    padding: 10px;
    max-width: 60px;
    cursor: pointer;
    :hover {
        background: lightYellow;
    }
`;
