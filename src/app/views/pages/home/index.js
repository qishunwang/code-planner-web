import React, { useState, useEffect } from "react";
import { } from "./styled";




function Home(props) {
    const { classes } = props;
    useEffect(() => {
        document.title = "Code Planner";

        return () => {
            document.title = "Loading...";
        };
    }, [props.isAuth]);


    return <div >
        <div style={{ display: 'block', height: 80 }}></div>
        <div style={{
            display: 'block',
            backgroundColor: 'white',
            position: 'fixed',
            width: '100%',
            top: 0,
            left: 0,
            zIndex: 500,
            paddingTop: '20px',
            paddingRight: '30px'
        }}>
            <div style={{
                display: 'flex'
            }}>
                CP</div>

        </div>
        <img src="./assets/img/template_folder.png" />

    </div>
}


export default Home