import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { sessionOperations } from "../../../state/ducks/session";
import { useHistory, useLocation, Redirect, Link } from "react-router-dom";
import { Grid, TextField, Button } from "@material-ui/core";
import styled from "styled-components";
import { StyledLink } from "../../components/StyledLink"
const Container = styled.div`
    padding: 40px;
`;
function Login(props) {
    let history = useHistory();
    let location = useLocation();
    let { isAuth, loginResult,isLoading } = props;
    const [submited, setSubmited] = useState(false);
    const textEl = useRef(null);
    let { from } = location.state || { from: { pathname: "/" } };
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    let onChange = type => e => {
        setSubmited(false)
        type == "username" ? setUsername(e.target.value) : null;
        type == "password" ? setPassword(e.target.value) : null;
    };
    let login = () => {
        setSubmited(true)
        props.login(username, password, () => {
            history.replace(from);
        });
    };
    useEffect(() => {
        document.title = "Login";
        document.body.style.backgroundColor = "#e6f2fa";
        if (submited && loginResult != null) {
            if (loginResult) {
                textEl.current.innerText = "👏🏼Login Success👏🏼"
                textEl.current.style.color = "green"
            } else {
                textEl.current.innerText = "🌬This account or password is incorrect.💨"
                textEl.current.style.color = "red"
            }
        } else {
            textEl.current.innerText = ""
        }
        if (isLoading) {
            textEl.current.innerText = "Loading..."
            textEl.current.style.color = "gray"
        }
        return () => {
            document.title = "Loading...";
            document.body.style.backgroundColor = null;
        };
    }, [loginResult, isLoading, submited]);

    return (
        <Container>
            {isAuth ? <Redirect to='/' /> : null}
            <p style={{ fontSize: '0.8em' }} ref={textEl}></p>
            <Grid container direction='column'>
                <TextField
                    placeholder='Email'
                    variant='outlined'
                    style={{ margin: "0 20px 20px 0" }}
                    onChange={onChange("username")}
                />
                <TextField
                    placeholder='Password'
                    variant='outlined'
                    style={{ margin: "0 20px 20px 0" }}
                    onChange={onChange("password")}
                />
                <Button
                    variant='outlined'
                    onClick={login}
                    style={{
                        margin: "0 20px 20px 0",
                        backgroundColor: "e6f2fa",
                    }}>
                    {"Login"}
                </Button>
                <Grid container justify="space-between" alignItems="center">
                    <StyledLink to="/registry">
                        <div style={{ color: 'gray', fontSize: '0.9em' }}>
                            {"沒有帳號？👆立即註冊👆"}
                        </div>
                    </StyledLink>
                    <StyledLink to="/forgot">
                        <div style={{ color: 'gray', fontSize: '0.9em' }}>
                            {"忘記密碼🥺?"}
                        </div>
                    </StyledLink>
                </Grid>

            </Grid>
        </Container>
    );
}

const mapStateToProps = state => ({
    isAuth: state.session.isAuth,
    isLoading: state.session.isLoading,
    loginResult: state.session.loginResult,
});

const mapDispatchToProps = dispatch => ({
    login: (username, password, cb) =>
        dispatch(sessionOperations.login(username, password, cb)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);
