import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { sessionOperations } from "../../../state/ducks/session";
import { useQuery } from "../utils"
import { StyledLink } from "../../components/StyledLink"
import { isNullOrUndefined } from "util";


function Verification(props) {
    const { classes, isValid, verify } = props;
    let token = useQuery().get("token");

    useEffect(() => {
        verify(token);
        document.title = "Verify";
        return () => {
            document.title = "Loading...";
        };
    }, [token]);

    return <div style={{ backgroundColor: 'black', width: '100vw', height: "100vh" }}>
        <img style={{ width: "100%", height: "auto" }} src="assets/img/email_header_image.png" />
        <p style={{ color: '#DFE200', textAlign: 'center', fontSize: '1.5em' }}>
            {
                isNullOrUndefined(isValid) ? null :
                    isValid ?
                        <StyledLink style={{ color: '#DFE200' }} to="/login">👏🏼Your account has activated!🙌 👉Login Now👈</StyledLink>
                        :
                        "🌬Your operation has expired. Please try again later💨"
            }
        </p>
    </div>
}

const mapStateToProps = state => ({
    isLoading: state.session.isLoading,
    isValid: state.session.verification
});

const mapDispatchToProps = dispatch => ({
    verify: (token) => {
        dispatch(sessionOperations.verify(token));
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Verification);
