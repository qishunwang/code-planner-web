import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { sessionOperations } from "../../../state/ducks/session";
import { useHistory, useLocation, Redirect, Link } from "react-router-dom";
import { Grid, TextField, Button } from "@material-ui/core";
import styled from "styled-components";

const Container = styled.div`
    padding: 40px;
`;
function Registry(props) {  
    let { isAuth, registerResult,isLoading } = props;
    const [submited, setSubmited] = useState(false);
    const textEl = useRef(null);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    let onChange = type => e => {
        setSubmited(false)
        type == "username" ? setUsername(e.target.value) : null;
        type == "password" ? setPassword(e.target.value) : null;
    };
    let register = () => {
        setSubmited(true)
        props.register(username, password);
    };
    useEffect(() => {
        document.title = "Login";
        document.body.style.backgroundColor = "#e6f2fa";
        if (submited && registerResult != null) {
            if (registerResult) {
                textEl.current.innerText = "👏🏼Register Success👏🏼"
                textEl.current.style.color = "green"
            } else {
                textEl.current.innerText = "🌬This Email or password is unavailable.💨"
                textEl.current.style.color = "red"
            }
        } else {
            textEl.current.innerText = ""
        }
        if (isLoading) {
            textEl.current.innerText = "Loading..."
            textEl.current.style.color = "gray"
        }
        return () => {
            document.title = "Loading...";
            document.body.style.backgroundColor = null;
        };
    }, [registerResult, isLoading, submited]);

    return (
        <Container> 
            <p style={{ fontSize: '0.8em' }} ref={textEl}></p>
            <Grid container direction='column'>
                <TextField
                    placeholder='Email'
                    helperText="where you will recieve a confirm email for account verification."
                    variant='outlined'
                    style={{ margin: "0 20px 20px 0" }}
                    onChange={onChange("username")}
                />
                <TextField
                    placeholder='Password'
                    variant='outlined'
                    style={{ margin: "0 20px 20px 0" }}
                    onChange={onChange("password")}
                />
                <Button
                    variant='outlined'
                    onClick={register}
                    disabled={isLoading||submited}
                    style={{
                        margin: "0 20px 20px 0",
                        backgroundColor: "e6f2fa",
                    }}>
                    {"Register"}
                </Button>
            </Grid>
        </Container>
    );
}

const mapStateToProps = state => ({ 
    isLoading: state.session.isLoading,
    registerResult: state.session.registerResult,
});

const mapDispatchToProps = dispatch => ({
    register: (username, password) =>
        dispatch(sessionOperations.register(username, password)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Registry);
