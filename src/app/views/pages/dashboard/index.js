import React, { Fragment, useState, useEffect } from "react";
import AppMenuContainer from "../../containers/AppMenuContainer";
import AppMain from "../../components/AppMain";
import AppWrapper from "../../components/AppWrapper";
import Login from "../login";
import Registry from "../registry";
import Forgot from "../forgot";
import Reset from "../reset";

import { connect } from "react-redux";
import { sessionOperations } from "../../../state/ducks/session";
import NewProject from "./components/NewProject";
import ListProject from "./components/ListProject";
import SelectedProject from "./components/SelectedProject";
import NewProjectModule from "./components/NewProjectModule";
import ListProjectModule from "./components/ListProjectModule";
import ModuleBucket from "./components/ModuleBucket";
import { Dialog, DialogContent } from '@material-ui/core'
import {
    Switch,
    Route,
    Redirect,
    useHistory,
    useLocation
} from "react-router-dom";

function Dashboard(props) {
    const { classes, isAuth } = props;
    const [project, setProject] = useState(null);
    const [module, setModule] = useState(null);
    const [modules, setModules] = useState([]);
    const [packageName, setPackageName] = useState("");
    const [dialog, setDialog] = useState(false);
    const [reload, setModuleListReload] = useState(false);
    let getInfo = () => {
        props.info();
    };
    useEffect(() => {
        document.title = "Dashboard";

        if (isAuth)
            getInfo();

        return () => {
            document.title = "Loading...";
        };
    }, [isAuth]);
    let selected = p => {
        console.log(p)
        setProject(p)
        setPackageName(p.packageName)
    }
    let selectedModule = m => {
        setModule(m)
        var newModules = [...modules]
        if (modules.find((module) => module.id == m.id)) {
            modules.map((item, i) => {
                if (item.id == m.id) {
                    newModules.splice(i, 1)
                }
            })
        } else {
            newModules.push(m)
        }
        setModules(newModules)
    }

    let openDialog = e => {
        setDialog(true)
    }
    let closeDialog = e => {
        setDialog(false)
        setModuleListReload(false)
    }
    let reloadProjectModule = () => {
        setModuleListReload(true)
    }
    return (
        <AppWrapper
            classes={classes}
            children={
                <Fragment>
                    <AppMenuContainer classes={classes} />
                    <AppMain
                        classes={classes}
                        children={
                            <Switch>
                                <Route path="/login"> {isAuth ? <Redirect to="/" /> : null}<Login /></Route>
                                <Route path="/registry">{isAuth ? <Redirect to="/" /> : null} <Registry /></Route>
                                <Route path="/forgot"> {isAuth ? <Redirect to="/" /> : null}<Forgot /></Route>
                                <Route path="/reset"> {isAuth ? <Redirect to="/" /> : null}<Reset /></Route>
                                <Route path="/project/list">
                                    {!isAuth ? <Redirect to="/login" /> : null}
                                    <ListProject handleSelectedProject={selected} />
                                    <SelectedProject project={project} />
                                    <Dialog open={dialog} onClose={closeDialog}>
                                        <DialogContent>
                                            <NewProjectModule project={project} handleSuccess={reloadProjectModule} />
                                        </DialogContent>
                                    </Dialog>
                                    <ListProjectModule newModuleReload={reload} handleSelectedModule={selectedModule} project={project} handleAddAction={openDialog} />
                                </Route>
                                <Route path="/project/new">
                                    {!isAuth ? <Redirect to="/login" /> : null}
                                    <NewProject />
                                </Route>
                                <Route path="/project">
                                    {!isAuth ? <Redirect to="/login" /> : null}
                                    <ListProject handleSelectedProject={selected} />
                                    <SelectedProject project={project} />
                                    <ListProjectModule handleSelectedModule={selectedModule} project={project} />
                                </Route>
                                <Route path="/tool">
                                    {!isAuth ? <Redirect to="/login" /> : null}
                                    <ListProject handleSelectedProject={selected} />
                                    <SelectedProject project={project} />
                                    <ListProjectModule handleSelectedModule={selectedModule} project={project} />
                                    <ModuleBucket modules={modules} packageName={packageName} />
                                </Route>
                                <Route path="/">
                                    <div>Tool</div>
                                </Route>
                            </Switch>
                        }
                    />
                </Fragment>
            }
        />
    );
}

const mapStateToProps = state => ({
    isAuth: state.session.isAuth,
    isLoading: state.session.isLoading,
});

const mapDispatchToProps = dispatch => ({
    info: () => {
        dispatch(sessionOperations.info());
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard);
