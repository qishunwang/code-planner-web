import React from 'react'

import './style.css'

export default (props) => {
    return <div className="loader" style={{ width: props.width, height: props.height }}></div>
}