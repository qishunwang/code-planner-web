import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { projectOperations } from "../../../../../state/ducks/project";
import Loading from "../Loading";
import { TextField, Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 300,
        },
        '& .MuiFilledInput-root': {
            backgroundColor: 'rgba(0, 0, 0, 0.0)'
        },
    },
}));
function NewProject(props) {
    const { loading, result } = props
    const classes = useStyles()
    const [name, setName] = useState("");
    const [packageName, setPackageName] = useState("");
    const [submited, setSubmited] = useState(false);
    const textEl = useRef(null);
    let update = e => {
        setName(e.target.value)
    }
    let updatePackageName = e => {
        setPackageName(e.target.value)
    }
    let create = (e) => {
        setSubmited(true)
        props.create(name, packageName)
    }
    useEffect(() => {
        document.title = "New Project";
        if (submited && result != null) {
            if (result) {
                textEl.current.innerText = "👏🏼Check your project list📁"
                textEl.current.style.color = "green"
            } else {
                textEl.current.innerText = "🌬Something went wrong.Try again later.💨"
                textEl.current.style.color = "red"
            }
        } else {
            textEl.current.innerText = ""
        }
        if (loading) {
            textEl.current.innerText = "Loading..."
            textEl.current.style.color = "gray"
        }
    }, [result, loading, submited])
    return (
        <div className={classes.root}>
            <h1>New Project</h1>
            <p style={{ fontSize: '0.8em' }} ref={textEl}></p>
            <Grid container
                direction="row"
                justify="flex-start"
                alignItems="stretch"
                spacing={4}>
                <Grid item  >
                    <TextField
                        required
                        label="Name"
                        variant="filled"
                        onChange={update}
                    />
                </Grid>
                <Grid item  >
                    <TextField
                        required
                        label="Android Package Name"
                        variant="filled"
                        helperText="com.example.template"
                        onChange={updatePackageName}
                    />
                </Grid>
            </Grid >
            <Button disabled={submited} style={{ margin: "30px" }} variant="outlined" color="secondary" onClick={create}>
                {"Create"}
            </Button>
        </div>
    )
}



const mapStateToProps = state => ({
    loading: state.project.loading,
    result: state.project.createProjectResult
});

const mapDispatchToProps = dispatch => ({
    create: (name, packageName) =>
        dispatch(projectOperations.createProject(name, packageName)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewProject);