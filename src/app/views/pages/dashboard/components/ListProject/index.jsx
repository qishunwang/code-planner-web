import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { projectOperations } from "../../../../../state/ducks/project";
import { Ul, Li, Item, IndexText, Title, Subtitle, LastLi } from "./styled"

function ListProject(props) {
    const { classes, list, loading, handleSelectedProject } = props
    const { projects = [], total, lower = 0, upper = 2 } = list
    const [page, setPage] = useState(0);
    let offset = 3
    let getList = (lower = 0, upper = lower + offset) => {
        if (page == 0) {
            props.listProject(lower, upper)
        } else {
            // console.log('lower=' + (lower + offset))
            // console.log('upper=' + (upper + offset))
            // console.log('offset=' + offset)
            props.listProject(lower + offset, upper + offset);
        }
    };
    let canLoadMore = upper < total
    let loadMore = () => {
        if (!canLoadMore) { return }
        let newPage = page + 1
        setPage(newPage)
    }
    useEffect(() => {
        getList(lower, upper);
        document.title = "List Project";
        return () => {
            document.title = "Loading...";
        };
    }, [page]);

    scroll = e => {
        var element = e.target;
        if (element.scrollHeight - element.scrollTop === element.clientHeight) {
            // console.log('scrolled');
            if (!loading) {
                setTimeout(
                    () => loadMore(), 1000
                )

                // console.log('load more');
            }
        }
    }

    let handleSelect = v => e => {
        handleSelectedProject(v)
    }
    return (
        <div >
            <h1>Projects</h1>
            <Ul onScroll={scroll}>
                {projects.map((project, i) =>
                    <Li
                        key={project.id}
                        onClick={handleSelect(project)}
                    >
                        <Item>
                            <IndexText>{i + 1}.</IndexText>
                            <Title>Name:{project.name}</Title>
                            <Subtitle>Android Package Name:{project.packageName}</Subtitle>
                        </Item>
                    </Li>
                )}
                <LastLi key={"More"} color={canLoadMore ? "gray" : 'lightGray'}>{canLoadMore ? "Load more" : "No More"}</LastLi>
            </Ul>
        </div>
    )
}

const mapStateToProps = state => ({
    list: state.project.list,
    loading: state.project.loading
});

const mapDispatchToProps = dispatch => ({
    listProject: (lower, upper) =>
        dispatch(projectOperations.listProject(lower, upper)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ListProject);
