import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { projectOperations } from "../../../../../state/ducks/project";
import { Ul, Li, Item, IndexText, Title, Subtitle, LastLi } from "./styled"
import { isNullOrUndefined } from "util";

function ListProjectModule(props) {
    const { classes, list, loading, project, handleSelectedModule, listProjectModule, handleAddAction, newModuleReload = false } = props
    const [reload, setReload] = useState(false);
    let listModule = (lower, upper, reload) => {
        if (project != null)
            listProjectModule(project.id, lower, upper, reload)
    }
    useEffect(() => {
        document.title = "Project";
        setReload(!reload)
        if (newModuleReload) {
            props.resetModuleList()
        }
        return () => {
            setReload(true)
            document.title = "Loading...";
        }
    }, [project, newModuleReload])
    return <ListModule handleSelectedModule={handleSelectedModule} listModule={listModule} list={list} reload={reload} newModuleReload={newModuleReload} handleAddAction={handleAddAction} />

}

function ListModule(props) {
    const { classes, list, loading, handleSelectedModule, listModule, reload, handleAddAction = null } = props
    const { modules = [], total, lower = 0, upper = 2 } = list
    const [page, setPage] = useState(0);
    let offset = 3
    let getList = (lower = 0, upper = lower + offset) => {
        if (page == 0) {
            listModule(lower, upper)
        } else {
            //     console.log('lower=' + (lower + offset))
            //     console.log('upper=' + (upper + offset))
            //     console.log('offset=' + offset)
            //     console.log('reload=' + reload)
            listModule(lower + offset, upper + offset);
        }
    };
    let canLoadMore = upper < total
    let loadMore = () => {
        if (!canLoadMore) { return }
        let newPage = page + 1
        setPage(newPage)
    }
    useEffect(() => {

        getList(lower, upper);
        if (reload) { setPage(0) }
    }, [page, reload]);

    scroll = e => {
        var element = e.target;
        if (element.scrollHeight - element.scrollTop === element.clientHeight) {
            // console.log('scrolled');
            if (!loading) {
                setTimeout(
                    () => loadMore(), 1000
                )
                // console.log('load more');
            }
        }
    }

    let handleSelect = v => e => {
        handleSelectedModule(v)
    }
    return (
        <div >
            <div className={"container"}>
                <div >
                    {"Modules"}
                </div>
                {
                    handleAddAction !== null ?
                        <img onClick={handleAddAction} style={{ cursor: 'pointer' }} src="assets/img/app_sub_menu_add.svg" />
                        : null
                }
            </div>

            <Ul onScroll={scroll}>
                {modules.map((module, i) =>
                    <Li
                        key={module.id}
                        onClick={handleSelect(module)}
                    >
                        <Item>
                            <IndexText>{i + 1}.</IndexText>
                            <Title>Name:{module.name}</Title>
                        </Item>
                    </Li>
                )}
                <LastLi key={"More"} color={canLoadMore ? "gray" : 'lightGray'}>{canLoadMore ? "Load more" : "No More"}</LastLi>
            </Ul>
        </div>
    )
}

const mapStateToProps = state => ({
    list: state.project.listModule,
    loading: state.project.loading
});

const mapDispatchToProps = dispatch => ({
    listProjectModule: (project_id, lower, upper) =>
        dispatch(projectOperations.listProjectModule(project_id, lower, upper)),
    resetModuleList: () =>
        dispatch(projectOperations.resetModuleList()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ListProjectModule);
