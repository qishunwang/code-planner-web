import styled from 'styled-components'

export const Ul = styled.ul`
  height: 200px;
  overflow-y: scroll;
  margin: 0;
  padding: 0;
  
  border: 2px solid #ccc;
  
  font-size: 16px;
  font-family: Arial, sans-serif;
  
  -webkit-overflow-scrolling: touch;

`;

export const Li = styled.li`
  border-bottom: 1px solid #ccc;
  &:last-child {
    border-bottom: none;
  }
  
  &:nth-child(even) {
    background: #f8f8f8;
  }  
`;

export const Item = styled.div`
padding: 10px 20px;
  
  :hover {
    background-color: #b1d7ff;
  }
  cursor: pointer;
`;
export const IndexText = styled.p`
-moz-user-select: none;
-khtml-user-select: none;
-webkit-user-select: none;
color: gray
`;
export const Title = styled.p`
-moz-user-select: none;
-khtml-user-select: none;
-webkit-user-select: none;
color: gray
`;
export const Subtitle = styled.p`
-moz-user-select: none;
-khtml-user-select: none;
-webkit-user-select: none;
font-size: 0.8em
color: lightGray
`;

export const LastLi = styled.li`
  text-align: center;
  font-size: 0.8em
  
  color: ${props => props.color}
  
  border-bottom: 1px solid #ccc;
  &:last-child {
    border-bottom: none;
  }
  
  &:nth-child(even) {
    background: #f8f8f8;
  }  
`;
