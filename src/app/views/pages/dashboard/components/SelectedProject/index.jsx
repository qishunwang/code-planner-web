import React from 'react'
import { isNullOrUndefined } from 'util'
import { Item, Title, Subtitle } from "./styled"

export default function SelectedProject(props) {
    let project = props.project
    var ele = null
    if (isNullOrUndefined(project)) {
        ele = <Title>Select a project</Title>
    } else {
        let { name, packageName } = props.project
        ele = <Item >
            <img style={{ position: "relative", 'top': "40px", "left": "-25px", height: '18px' }} src="../assets/img/pointing-right.svg" />
            <Title>{name}</Title>
            <Subtitle>{packageName}</Subtitle>
        </Item>
    }
    return ele
}