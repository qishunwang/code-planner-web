import styled from 'styled-components'
export const Item = styled.div`
padding: 30px 30px;
cursor: pointer;
`;

export const Title = styled.p`
-moz-user-select: none;
-khtml-user-select: none;
-webkit-user-select: none;
color: gray
`;
export const Subtitle = styled.p`
-moz-user-select: none;
-khtml-user-select: none;
-webkit-user-select: none;
font-size: 0.8em
color: lightGray
`;
