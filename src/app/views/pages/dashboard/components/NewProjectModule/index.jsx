import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { projectOperations } from "../../../../../state/ducks/project";
import Loading from "../Loading";
import { isNullOrUndefined } from "util";
import { TextField, Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 300,
        },
        '& .MuiFilledInput-root': {
            backgroundColor: 'rgba(0, 0, 0, 0.0)'
        },
    },
}));

function NewProjectModule(props) {
    const { loading, project, result, handleSuccess } = props
    const classes = useStyles();
    const [name, setName] = useState("");
    const [submited, setSubmited] = useState(false);
    const textEl = useRef(null);

    let update = e => {
        setName(e.target.value)
    }
    let create = (e) => {
        setSubmited(true)
        let { id } = project
        props.create(id, name)
    }
    useEffect(() => {
        document.title = "New Module";
        if (submited && result != null) {
            if (result) {
                textEl.current.innerText = "👏🏼Check your Module list📁"
                textEl.current.style.color = "green"
                handleSuccess()
            } else {
                textEl.current.innerText = "🌬Something went wrong.Try again later.💨"
                textEl.current.style.color = "red"
            }
        } else {
            textEl.current.innerText = ""
        }
        if (loading) {
            textEl.current.innerText = "Loading..."
            textEl.current.style.color = "gray"
        }
        if (isNullOrUndefined(project)) {
            textEl.current.innerText = "Select a project first!"
        }
    }, [result, loading, submited])

    var ele = <p style={{ fontSize: '0.8em' }} ref={textEl}></p>
    if (!isNullOrUndefined(project)) {
        ele = <div style={{ padding: 10 }}>
            <h1>New Module</h1>
            <p style={{ fontSize: '0.8em' }} ref={textEl}></p>
            <Grid container
                direction="row"
                justify="flex-start"
                alignItems="center"
                spacing={4}>
                <Grid item className={classes.root} >
                    <TextField
                        style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}
                        required
                        label="Name"
                        variant="filled"
                        helperText={"capitalize with whitespace if you got more than one word like \"UserList\" as \"User List\""}
                        onChange={update}
                    />
                </Grid>
                <Grid item>
                    <Button disabled={submited} variant="outlined" color="secondary" onClick={create}>
                        {"Create"}
                    </Button>
                </Grid>
            </Grid>
        </div>
    }
    return ele
}



const mapStateToProps = state => ({
    loading: state.project.loading,
    result: state.project.createModuleResult
});

const mapDispatchToProps = dispatch => ({
    create: (project_id, name) =>
        dispatch(projectOperations.createProjectModule(project_id, name)),

});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewProjectModule);