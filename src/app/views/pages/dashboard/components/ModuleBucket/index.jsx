import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { projectOperations } from "../../../../../state/ducks/project";
import { isNullOrUndefined } from 'util'
import { Link } from "react-router-dom";
import { Grid, Button } from '@material-ui/core';
function ModuleBucket(props) {
    let { modules, downloadToken = "", packageName = "com.example.tempate" } = props
    const [warning, setWarning] = useState(false);

    let generate = (e) => {
        var ids = []
        modules.map(item => {
            ids.push(item.id)
        })
        if (ids.length == 0) {
            setWarning(true)
            return
        }
        props.generateModules(ids, packageName)
    }
    let download = e => {
        props.download(downloadToken)
    }

    useEffect(() => {
        document.title = "Tool";
        setWarning(false)
        return () => {
            document.title = "Loading...";
        }
    }, [modules])
    return <div>
        {modules.map((module, i) =>
            <div
                key={module.id}
            // onClick={handleSelect(module)}
            >{i + 1}.名稱:{module.name}</div>
        )}
        {
            warning ?
                <p style={{ color: 'red' }}>At least select a module</p>
                : null
        }

        <div style={{ padding: 18 }}>
            {downloadToken !== ""
                ?
                <Button variant="outlined" color="primary" onClick={download}>Download</Button>
                :
                <Button variant="outlined" color="secondary" onClick={generate}>生成</Button>
            }
        </div>
    </div>
}

const mapStateToProps = state => ({
    downloadToken: state.project.token
});

const mapDispatchToProps = dispatch => ({
    generateModules: (module_ids = [], packageName = "com.example.template") =>
        dispatch(projectOperations.generateModules(module_ids, packageName)),
    download: (token) => dispatch(projectOperations.downloadModules(token)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ModuleBucket);
