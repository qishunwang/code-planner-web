import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { sessionOperations } from "../../../state/ducks/session";
import { useHistory, useLocation, Redirect, Link } from "react-router-dom";
import { Grid, TextField, Button } from "@material-ui/core";
import styled from "styled-components";
import { useQuery } from "../utils"

const Container = styled.div`
    padding: 40px;
`;
function Forgot(props) {
    const [username, setUsername] = useState("");
    const [submited, setSubmited] = useState(false);
    const textEl = useRef(null);
    let token = useQuery().get("token");
    let onChange = type => e => {
        type == "username" ? setUsername(e.target.value) : null;
    };

    let forgot = () => {
        setSubmited(true)
        props.forgot(username);
    };
    useEffect(() => {
        document.title = "Forgot Password";
        document.body.style.backgroundColor = "#e6f2fa";

        if (submited && props.result != null) {
            if (props.result) {
                textEl.current.innerText = "👏🏼Check your Email📨"
                textEl.current.style.color = "green"
            } else {
                textEl.current.innerText = "🌬This account doesn't exist.💨"
                textEl.current.style.color = "red"
            }
        } else {
            textEl.current.innerText = "Help Me"
        }
        if (props.isLoading) {
            textEl.current.innerText = "Loading..."
            textEl.current.style.color = "gray"
        }
        return () => {
            document.title = "Loading...";
            document.body.style.backgroundColor = null;
        };
    }, [props.result, props.isLoading, submited]);
    return (
        <Container>
            {token != null ? <Redirect to={{ pathname: "/reset", state: { token: token } }} /> : null}

            <Grid container direction='column'>
                <TextField
                    placeholder='Email'
                    helperText="you will recieve a hint for the account registered by this Email."
                    variant='outlined'
                    style={{ margin: "0 20px 20px 0" }}
                    onChange={onChange("username")}
                />
                <Button
                    variant='outlined'
                    onClick={forgot}
                    disabled={submited}
                    style={{
                        margin: "0 20px 0 0",
                        backgroundColor: "e6f2fa",
                    }}>
                    <p style={{ fontSize: '0.8em' }} ref={textEl}></p>
                </Button>
            </Grid>
        </Container>
    );
}

const mapStateToProps = state => ({
    isLoading: state.session.isLoading,
    result: state.session.forgotResult,
});

const mapDispatchToProps = dispatch => ({
    forgot: (username) =>
        dispatch(sessionOperations.forgot(username)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Forgot);
