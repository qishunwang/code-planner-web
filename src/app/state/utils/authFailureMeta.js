import commonFailureMeta from "./commonFailureMeta";

export default (_401cb) => (action, state, res) => {

    if (res.status === 401) {
        _401cb()
    }
    return commonFailureMeta(action, state, res)
}
