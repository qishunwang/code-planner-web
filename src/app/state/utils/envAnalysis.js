import CryptoJS from 'crypto-js';

function analysisAPI() {
    var token = window.env.app
    var bytes = CryptoJS.AES.decrypt(token, 'cp-aes-frontend');
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    const { domain, api } = JSON.parse(originalText)
    return api 
}
export const api_address = analysisAPI()