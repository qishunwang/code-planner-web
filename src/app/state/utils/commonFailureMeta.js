export default (action, state, res) => {
    if (res) {
        return {
            status: res.status,
            statusText: res.statusText
        }
    } else {
        return {
            status: 'Network request failed'
        }
    }
}
