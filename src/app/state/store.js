import { createStore, applyMiddleware, combineReducers, compose } from "redux";

import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import * as reducers from "./ducks";
import { apiMiddleware } from "redux-api-middleware";
import thunkMiddleware from "redux-thunk"
import { createLogger } from "./middlewares";

const persistConfig = {
    key: "root",
    storage: storage,
    stateReconciler: autoMergeLevel2,
};
export default function configureStore(initialState) {
    const rootReducer = combineReducers(reducers);
    // const persistRootReducer = persistReducer(persistConfig, rootReducer)
    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(apiMiddleware, thunkMiddleware, createLogger(true))
    );
}
