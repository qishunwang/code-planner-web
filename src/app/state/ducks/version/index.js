import reducer from "./reducers"

import * as versionOperations from "./operations"

export {
    versionOperations,
}

export default reducer