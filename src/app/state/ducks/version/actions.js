import * as types from "./types"
import { RSAA } from 'redux-api-middleware'
import { commonFailureMeta, commonHeaders } from "../../utils"
import { isUndefined } from "util";

export const checkVersion = (current_version) => {
    return async (dispatch, getState) => {
        const actionResponse = await dispatch({
            [RSAA]: {
                endpoint: process.env.api + '/api/v1/version/check?version=' + current_version,
                method: 'GET',
                headers: commonHeaders,
                options: { timeout: 3000 },
                credentials: 'include',
                types: [
                    types.CHECK_VERSION,
                    types.CHECK_VERSION_COMPLETED,
                    {
                        type: types.CHECK_VERSION_FAILED,
                        meta: commonFailureMeta
                    }
                ]
            }
        })

        //Ref - Dispatching Thunks[https://github.com/agraboso/redux-api-middleware]
        if (actionResponse.error) {
            throw new Error("Promise flow received action error", actionResponse)
        }
        // return await dispatch(redirect("/dashboard"))
    }
}