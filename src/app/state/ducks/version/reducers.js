import { combineReducers } from "redux"
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import * as types from "./types"
import { createReducer } from "../../utils"

const authPersistConfig = {
    key: 'frontend',
    storage: storage
}

export default persistReducer(authPersistConfig, frontendReducer)