import * as types from "./types";
import { RSAA } from "redux-api-middleware";
import { commonFailureMeta, commonHeaders, authFailureMeta } from "../../utils";
import { isUndefined } from "util";
import { api_address } from "../../utils/envAnalysis"

export function listProject(lower, upper) {
    const l = (lower === undefined) ? 0 : lower
    const u = (upper === undefined) ? 0 : upper

    return {
        [RSAA]: {
            endpoint: api_address + '/v1/admin/projects' + "?lower=" + l + "&upper=" + u,
            method: "GET",
            headers: commonHeaders,
            options: { timeout: 3000 },
            credentials: "include",
            types: [
                types.LIST_PROJECT,
                types.LIST_PROJECT_COMPLETED,
                {
                    type: types.LIST_PROJECT_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};

export function createProject(name, packageName) {
    return {
        [RSAA]: {
            endpoint: api_address + "/v1/admin/projects",
            method: "POST",
            headers: commonHeaders,
            body: JSON.stringify({
                name: name,
                packageName: packageName
            }),
            options: { timeout: 3000 },
            credentials: "include",
            types: [
                types.CREATE_PROJECT,
                types.CREATE_PROJECT_COMPLETED,
                {
                    type: types.CREATE_PROJECT_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};
export const resetModuleList = () => ({
    type: types.RESET_MODULE_LIST,
});
export function createProjectModule(project_id, name) {
    return {
        [RSAA]: {
            endpoint: api_address + "/v1/admin/projects/" + project_id + "/modules",
            method: "POST",
            headers: commonHeaders,
            body: JSON.stringify({
                name: name
            }),
            options: { timeout: 3000 },
            credentials: "include",
            types: [
                types.CREATE_PROJECT_MODULE,
                types.CREATE_PROJECT_MODULE_COMPLETED,
                {
                    type: types.CREATE_PROJECT_MODULE_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};

export function listProjectModule(project_id, lower, upper) {
    const l = (lower === undefined) ? 0 : lower
    const u = (upper === undefined) ? 2 : upper

    return {
        [RSAA]: {
            endpoint: api_address + '/v1/admin/projects/' + parseInt(project_id) + '/modules' + "?lower=" + l + "&upper=" + u,
            method: "GET",
            headers: commonHeaders,
            options: { timeout: 3000 },
            credentials: "include",
            types: [
                types.LIST_PROJECT_MODULE,
                types.LIST_PROJECT_MODULE_COMPLETED,
                {
                    type: types.LIST_PROJECT_MODULE_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};


export function generateModules(ids = [], packageName) {
    return {
        [RSAA]: {
            endpoint: api_address + '/v1/admin/template',
            method: "POST",
            headers: commonHeaders,
            options: { timeout: 3000 },
            body: JSON.stringify({ 'ids': ids.toString(), 'package': packageName }),
            credentials: "include",
            types: [
                types.GENERATE_MODULES,
                types.GENERATE_MODULES_COMPLETED,
                {
                    type: types.GENERATE_MODULES_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};

export function downloadModules(token) {

    function saveData(blob, fileName) {
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";

        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
    }

    return async (dispatch, getState) => {

        dispatch({ type: types.DOWNLOAD_MODULES })
        var xhr = new XMLHttpRequest();
        xhr.open("GET", api_address + '/v1/admin/template/' + token);
        xhr.responseType = "blob";
        xhr.withCredentials = true
        xhr.onload = function () {
            if (xhr.status == 200) {
                saveData(xhr.response, 'archived.zip');
                dispatch({ type: types.DOWNLOAD_MODULES_COMPLETED })
            } else {
                dispatch({ type: types.DOWNLOAD_MODULES_FAILED })
            }

        };
        xhr.send();
    }
};
