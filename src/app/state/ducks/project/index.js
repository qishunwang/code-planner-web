import reducer from "./reducers"

import * as projectOperations from "./operations"

export {
    projectOperations,
}

export default reducer