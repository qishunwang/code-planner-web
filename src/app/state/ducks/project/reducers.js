import { combineReducers } from "redux";
import * as types from "./types";
import { createReducer } from "../../utils";

let P = { lower: 0, upper: 2, total: 0, projects: [] }
let M = { lower: 0, upper: 2, total: 0, modules: [] }
const listReducer = createReducer(P)({
    [types.LIST_PROJECT]: (state, action) => state,
    [types.LIST_PROJECT_FAILED]: (state, action) => state,
    [types.LIST_PROJECT_COMPLETED]: (state, action) => {
        var dups = [];
        var newArray = [...state.projects, ...action.payload.projects]
        var arr = newArray.filter(function (el) {
            // If it is not a duplicate, return true
            if (dups.indexOf(el.id) == -1) {
                dups.push(el.id);
                return true;
            }
            return false;
        });

        return { ...action.payload, projects: arr }
    },
});

const listModuleReducer = createReducer(M)({
    [types.LIST_PROJECT_MODULE]: (state, action) => state,
    [types.LIST_PROJECT_MODULE_FAILED]: (state, action) => state,
    [types.RESET_MODULE_LIST]: (state, action) => M,
    [types.LIST_PROJECT_MODULE_COMPLETED]: (state, action) => {
        var dups = [];
        var newArray = [...state.modules, ...action.payload.modules]
        var arr = newArray.filter(function (el) {
            // If it is not a duplicate, return true
            if (dups.indexOf(el.id) == -1) {
                dups.push(el.id);
                return true;
            }
            return false;
        });
        return { ...action.payload, modules: arr }
    },
});
const loadingReducer = createReducer(false)({
    [types.LIST_PROJECT]: () => true,
    [types.LIST_PROJECT_FAILED]: () => false,
    [types.LIST_PROJECT_COMPLETED]: () => false,

    [types.CREATE_PROJECT]: () => true,
    [types.CREATE_PROJECT_FAILED]: () => false,
    [types.CREATE_PROJECT_COMPLETED]: () => false,

    [types.LIST_PROJECT_MODULE]: () => true,
    [types.LIST_PROJECT_MODULE_FAILED]: () => false,
    [types.LIST_PROJECT_MODULE_COMPLETED]: () => false,

    [types.CREATE_PROJECT_MODULE]: () => true,
    [types.CREATE_PROJECT_MODULE_FAILED]: () => false,
    [types.CREATE_PROJECT_MODULE_COMPLETED]: () => false,
});

const createProjectReducer = createReducer(null)({
    [types.CREATE_PROJECT]: (state, action) => null,
    [types.CREATE_PROJECT_FAILED]: (state, action) => false,
    [types.CREATE_PROJECT_COMPLETED]: (state, action) => true,
});
const createModuleReducer = createReducer(null)({
    [types.CREATE_PROJECT_MODULE]: (state, action) => null,
    [types.CREATE_PROJECT_MODULE_FAILED]: (state, action) => false,
    [types.CREATE_PROJECT_MODULE_COMPLETED]: (state, action) => true,
});

const templateTokenReducer = createReducer("")({
    [types.GENERATE_MODULES]: (state, action) => "",
    [types.GENERATE_MODULES_FAILED]: (state, action) => "",
    [types.GENERATE_MODULES_COMPLETED]: (state, action) => action.payload.token,
});
const downloadReducer = createReducer("")({
    [types.DOWNLOAD_MODULES]: (state, action) => "",
    [types.DOWNLOAD_MODULES_FAILED]: (state, action) => "",
    [types.DOWNLOAD_MODULES_COMPLETED]: (state, action) => action,
});

const projectReducer = combineReducers({
    loading: loadingReducer,
    list: listReducer,
    listModule: listModuleReducer,
    token: templateTokenReducer,
    createProjectResult: createProjectReducer,
    createModuleResult: createModuleReducer
});

export default projectReducer
