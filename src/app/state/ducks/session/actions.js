import * as types from "./types";
import { RSAA } from "redux-api-middleware";
import { commonFailureMeta, commonHeaders, authFailureMeta } from "../../utils";
import { isUndefined } from "util";
import { api_address } from "../../utils/envAnalysis"


export function reset(token, password) {
    return {
        [RSAA]: {
            endpoint: api_address + "/v1/admin/reset",
            method: "POST",
            headers: commonHeaders,
            options: { timeout: 3000 },
            body: JSON.stringify({
                token: token,
                password: password
            }),
            credentials: "include",
            types: [
                types.RESET,
                types.RESET_COMPLETED,
                {
                    type: types.RESET_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};
export function forgot(username) {
    return {
        [RSAA]: {
            endpoint: api_address + "/v1/admin/forgot",
            method: "POST",
            headers: commonHeaders,
            options: { timeout: 3000 },
            body: JSON.stringify({
                username: username,
            }),
            credentials: "include",
            types: [
                types.FORGOT,
                types.FORGOT_COMPLETED,
                {
                    type: types.FORGOT_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};
export function register(username, password) {
    return {
        [RSAA]: {
            endpoint: api_address + "/v1/admin/registry",
            method: "POST",
            headers: commonHeaders,
            options: { timeout: 3000 },
            body: JSON.stringify({
                username: username,
                password: password,
            }),
            credentials: "include",
            types: [
                types.REGISTER,
                types.REGISTER_COMPLETED,
                {
                    type: types.REGISTER_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};

export function verify(token) {
    return {
        [RSAA]: {
            endpoint: api_address + "/v1/admin/verify",
            method: "POST",
            headers: commonHeaders,
            options: { timeout: 3000 },
            body: JSON.stringify({
                token: token
            }),
            credentials: "include",
            types: [
                types.VERIFY,
                types.VERIFY_COMPLETED,
                {
                    type: types.VERIFY_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};

export function login(username, password) {
    return {
        [RSAA]: {
            endpoint: api_address + "/v1/admin/signin",
            method: "POST",
            headers: commonHeaders,
            options: { timeout: 3000 },
            body: JSON.stringify({
                username: username,
                password: password,
            }),
            credentials: "include",
            types: [
                types.LOGIN,
                types.LOGIN_COMPLETED,
                {
                    type: types.LOGIN_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        }
    }
};
export function info() {
    return async (dispatch, getState) =>
        dispatch(
            {
                [RSAA]: {
                    endpoint: api_address + "/v1/admin/info",
                    method: "GET",
                    headers: commonHeaders,
                    credentials: "include",
                    types: [
                        types.INFO,
                        types.INFO_COMPLETED,
                        {
                            type: types.INFO_FAILED,
                            meta: authFailureMeta(() => dispatch(logout())),
                        },
                    ],
                }
            }
        )
};

export function logout() {

    return {
        [RSAA]: {
            endpoint: api_address + "/v1/admin/signout",
            method: "GET",
            headers: commonHeaders,
            credentials: "include",
            types: [
                types.LOGOUT,
                types.LOGOUT_COMPLETED,
                {
                    type: types.LOGOUT_FAILED,
                    meta: commonFailureMeta,
                },
            ],
        },
    }
};
