import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import * as types from "./types";
import { createReducer } from "../../utils";
/* State shape
{
    redirectPath: string,
    isAuth: bool,
    error: bool,
    reason: string,
    result: {},
}
*/

//Auth
const authReducer = createReducer(false)({
    [types.LOGIN]: () => false,
    [types.LOGIN_FAILED]: () => false,
    [types.LOGIN_COMPLETED]: () => true,
    [types.LOGOUT_COMPLETED]: () => false,
    [types.LOGOUT_FAILED]: () => false,
    // [types.INFO_FAILED]: () => false,
});
// Info
const infoReducer = createReducer({})({
    [types.INFO_COMPLETED]: (state, action) => action.payload,
});

const verificationReducer = createReducer(null)({
    [types.VERIFY]: (state, action) => null,
    [types.VERIFY_COMPLETED]: (state, action) => true,
    [types.VERIFY_FAILED]: (state, action) => false,
});
const forgotReducer = createReducer(null)({
    [types.FORGOT]: (state, action) => null,
    [types.FORGOT_COMPLETED]: (state, action) => true,
    [types.FORGOT_FAILED]: (state, action) => false,
});
const loginReducer = createReducer(null)({
    [types.LOGIN]: (state, action) => null,
    [types.LOGIN_COMPLETED]: (state, action) => true,
    [types.LOGIN_FAILED]: (state, action) => false,
});

const registerReducer = createReducer(null)({
    [types.REGISTER]: (state, action) => null,
    [types.REGISTER_COMPLETED]: (state, action) => true,
    [types.REGISTER_FAILED]: (state, action) => false,
});
const resetReducer = createReducer(null)({
    [types.RESET]: (state, action) => null,
    [types.RESET_COMPLETED]: (state, action) => true,
    [types.RESET_FAILED]: (state, action) => false,
});


//Loading
const loadingReducer = createReducer(false)({
    [types.LOGIN]: () => true,
    [types.LOGIN_FAILED]: () => false,
    [types.LOGIN_COMPLETED]: () => false,

    [types.LOGOUT]: () => true,
    [types.LOGOUT_COMPLETED]: () => false,
    [types.LOGOUT_FAILED]: () => false,

    [types.INFO]: () => true,
    [types.INFO_COMPLETED]: () => false,
    [types.INFO_FAILED]: () => false,

    [types.FORGOT]: () => true,
    [types.FORGOT_COMPLETED]: () => false,
    [types.FORGOT_FAILED]: () => false,

    [types.REGISTER]: () => true,
    [types.REGISTER_COMPLETED]: () => false,
    [types.REGISTER_FAILED]: () => false,

    [types.RESET]: () => true,
    [types.RESET_COMPLETED]: () => false,
    [types.RESET_FAILED]: () => false,
});

const sessionReducer = combineReducers({
    isAuth: authReducer,
    info: infoReducer,
    isLoading: loadingReducer,
    verification: verificationReducer,
    forgotResult: forgotReducer,
    loginResult: loginReducer,
    registerResult: registerReducer,
    resetResult: resetReducer,
});

const authPersistConfig = {
    key: "auth",
    storage: storage,
};

export default persistReducer(authPersistConfig, sessionReducer);
