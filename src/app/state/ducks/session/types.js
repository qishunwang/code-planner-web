export const LOGIN = "session/LOGIN";
export const LOGIN_COMPLETED = "session/LOGIN_COMPLETED";
export const LOGIN_FAILED = "session/LOGIN_FAILED";

export const INFO = "session/INFO";
export const INFO_COMPLETED = "session/INFO_COMPLETED";
export const INFO_FAILED = "session/INFO_FAILED";

export const LOGOUT = "session/LOGOUT";
export const LOGOUT_COMPLETED = "session/LOGOUT_COMPLETED";
export const LOGOUT_FAILED = "session/LOGOUT_FAILED";


export const VERIFY = "session/VERIFY";
export const VERIFY_COMPLETED = "session/VERIFY_COMPLETED";
export const VERIFY_FAILED = "session/VERIFY_FAILED";

export const REGISTER = "session/REGISTER";
export const REGISTER_COMPLETED = "session/REGISTER_COMPLETED";
export const REGISTER_FAILED = "session/REGISTER_FAILED";

export const FORGOT = "session/FORGOT";
export const FORGOT_COMPLETED = "session/FORGOT_COMPLETED";
export const FORGOT_FAILED = "session/FORGOT_FAILED";

export const RESET = "session/RESET";
export const RESET_COMPLETED = "session/RESET_COMPLETED";
export const RESET_FAILED = "session/RESET_FAILED";
