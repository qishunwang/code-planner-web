export {INFO_COMPLETED}  from "../session/types"
export const RESET_APP_MENU_STRUCT = "frontend/RESET_APP_MENU_STRUCT";
export const GET_APP_MENU_STRUCT = "frontend/GET_APP_MENU_STRUCT";
export const EXTEND_APP_MENU = "frontend/EXTEND_APP_MENU";
export const GET_CURRENT_APP_MAIN = "frontend/GET_CURRENT_APP_MAIN";
export const RESET_APP = "frontend/RESET_APP";
