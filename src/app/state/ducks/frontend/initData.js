import Icon1 from "../../../../assets/img/app_menu_project_management_icon.svg"
import Icon2 from "../../../../assets/img/app_sub_menu_add.svg"
import Icon3 from "../../../../assets/img/app_sub_menu_list.svg"
import Icon4 from "../../../../assets/img/app_menu_tool_management.svg"

export const InitAppVersion = "0.0.0";

export const InitAppMenuStruct = [
    {
        id: "0",
        title: "專案管理",
        link: "/project",
        img: Icon1,
        isExtend: false,
        children: [
            {
                id: "0",
                title: "新增專案",
                img: Icon2,
                link: "/project/new",
            },
            {
                id: "1",
                title: "專案列表",
                img: Icon3,
                link: "/project/list",
            },
        ],
    },
    {
        id: "0",
        title: "生成工具",
        link: "/tool",
        img: Icon4,
        isExtend: false,
        children: [],
    },

];

export const InitAppMainState = {
    index: 0,
    pages: [{ key: "profile" }, { key: "feed" }],
};
