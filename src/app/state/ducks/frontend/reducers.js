import { combineReducers } from "redux"
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import * as types from "./types"
import { createReducer } from "../../utils"
import { InitAppMenuStruct, InitAppMainState } from "./initData"
//[Ref]:https://redux.js.org/recipes/structuring-reducers/immutable-update-patterns
const appMenuStructReducer = createReducer(InitAppMenuStruct)({
    [types.EXTEND_APP_MENU]: (state, action) => {
        return state.map((item, index) => {
            if (index !== action.payload) {
                // This isn't the item we care about - keep it as-is
                return item
            }
            // Otherwise, this is the one we want - return an updated value
            return {
                ...item,
                isExtend: !item.isExtend
            }
        })
    },
    [types.GET_APP_MENU_STRUCT]: (state, action) => state,
    [types.RESET_APP_MENU_STRUCT]: (state, action) => InitAppMenuStruct,
    [types.RESET_APP]: (state, action) => InitAppMenuStruct,
})

const appMainStateReducer = createReducer(InitAppMainState)({
    [types.GET_CURRENT_APP_MAIN]: (state, action) => state,
    [types.RESET_APP]: (state, action) => InitAppMainState,
})

const adminInfoStateReducer = createReducer({})({
    [types.INFO_COMPLETED]: (state, action) => action.payload
    
})

const frontendReducer = combineReducers({
    appMenuStruct: appMenuStructReducer,
    appMainState: appMainStateReducer,
    adInfo: adminInfoStateReducer
})


const authPersistConfig = {
    key: 'frontend',
    storage: storage
}

export default persistReducer(authPersistConfig, frontendReducer)