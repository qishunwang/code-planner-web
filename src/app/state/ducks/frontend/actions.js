import * as types from "./types";

export const resetApp = () => ({
    type: types.RESET_APP,
});

export const resetAppMenuStruct = () => ({
    type: types.RESET_APP_MENU_STRUCT,
});

export const extendAppMenu = index => ({
    type: types.EXTEND_APP_MENU,
    payload: index,
});

export const getAppMenu = () => ({
    type: types.GET_APP_MENU_STRUCT,
});

export const getCurrentAppMain = () => ({
    type: types.GET_CURRENT_APP_MAIN,
});
