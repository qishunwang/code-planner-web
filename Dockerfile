# => Build container
FROM node:alpine as builder
WORKDIR /app
COPY package.json .
RUN yarn
COPY . .
RUN yarn build

# => Run container
FROM nginx:1.15.2-alpine

# Nginx config
# RUN rm -rf /etc/nginx/conf.d
# COPY conf /etc/nginx

# Static build
COPY --from=builder /app/build /usr/share/nginx/html

# Default port exposure
EXPOSE 80

# Copy .env file and shell script to container
WORKDIR /usr/share/nginx/html
COPY ./run .

# Add bash
RUN apk add --no-cache bash

# Make our shell script executable
RUN chmod +x run

# Start Nginx server
CMD ["/bin/bash", "-c", "./run ./assets/env-config.js && nginx -g \"daemon off;\""]