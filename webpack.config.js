const HtmlWebPackPlugin = require("html-webpack-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")

const htmlPlugin = new HtmlWebPackPlugin({
    template: "./src/root/index.html",
    filename: "./index.html"
})
const copyWebpackPlugin = new CopyWebpackPlugin([{ from: 'src/assets', to: 'assets' }])
const path = require('path')
module.exports = {
    entry: ["./src/root"],
    output: {
        path: path.resolve('build'),
        filename: '[hash].bundled.js',
        chunkFilename: '[name].bundle.js'
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
        },
    },
    resolve: {
        extensions: ['*', '.js', '.jsx', '.json']
    },
    devServer: {
        historyApiFallback: true,
    },

    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg|json|ico|ttf|mp3|woff)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {},
                    },
                ],
            }
        ]
    },
    plugins: [
        htmlPlugin,
        copyWebpackPlugin,
    ]
};